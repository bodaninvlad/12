package com.stormnet.figuresFx.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.Objects;

public class Triangle extends Figure {

    private double a;
    private double h;
    private double r;

    public Triangle(double lineWidth, Color color, double x, double y, double a) {
        this(lineWidth, color, x, y);
        this.a = a;
        h = a * Math.sqrt(3) / 2;
        r = a / Math.sqrt(3);
    }

    private Triangle(double lineWidth, Color color, double x, double y) {
        super(lineWidth, color, x, y, FIGURE_TYPE_TRIANGLE);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Triangle triangle = (Triangle) o;
        return Double.compare(triangle.a, a) == 0 &&
                Double.compare(triangle.h, h) == 0 &&
                Double.compare(triangle.r, r) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(a, h, r);
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setLineWidth(lineWidth);
        gc.beginPath();
        gc.setFill(color);
        gc.moveTo(x, y - r);
        gc.lineTo(x + r, y + h - r);
        gc.lineTo(x - r, y + h - r);
        gc.lineTo(x, y - r);
        gc.closePath();
        gc.stroke();
    }
}
