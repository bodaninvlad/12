package com.stormnet.figuresFx.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public abstract class Figure {

    protected double lineWidth;
    protected Color color;
    protected double x;
    protected double y;

    final static public int FIGURE_TYPE_CIRCLE = 0;
    final static public int FIGURE_TYPE_RECTANGLE = 1;
    final static public int FIGURE_TYPE_TRIANGLE = 2;
    final static public int FIGURE_TYPE_SIN = 3;
    final static public int FIGURE_TYPE_SOME_FIGURE = 4;

    private int type;

    public abstract void draw(GraphicsContext gc);

    public Figure(double lineWidth, Color color, double x, double y, int type) {
        this.lineWidth = lineWidth;
        this.color = color;
        this.x = x;
        this.y = y;
        this.type = type;
    }

    public double getLineWidth() {
        return lineWidth;
    }

    public void setLineWidth(double lineWidth) {
        this.lineWidth = lineWidth;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public int getType() {
        return type;
    }

}
