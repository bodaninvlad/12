package com.stormnet.figuresFx.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Sin extends Figure {

    public static final int PERIOD = 4;

    public Sin(double lineWidth, Color color, double x, double y) {
        super(lineWidth, color, x, y, FIGURE_TYPE_SIN);
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setLineWidth(lineWidth);

        for (double i = 0; i < PERIOD; i += 0.01) {
            gc.beginPath();
            gc.moveTo(x - 100 + i * 50, y + 20 * Math.sin(i * Math.PI));
            gc.setFill(color);
            gc.closePath();
            gc.stroke();
        }
    }
}
