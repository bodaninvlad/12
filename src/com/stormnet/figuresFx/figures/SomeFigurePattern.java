package com.stormnet.figuresFx.figures;

import com.stormnet.figuresFx.Exceptions.IndexBoundException;
import com.stormnet.figuresFx.Exceptions.NullFigureException;
import com.stormnet.figuresFx.controller.Controller;
import javafx.scene.paint.Color;
import org.apache.log4j.Logger;

import java.util.ArrayList;

public abstract class SomeFigurePattern extends Figure {
    static private ArrayList<Double> xArray;
    static private ArrayList<Double> yArray;
    static private double cx;
    static private double cy;

    public static ArrayList<Double> getxArray() {
        return xArray;
    }

    public static ArrayList<Double> getyArray() {
        return yArray;
    }

    private static final Logger log = Logger.getLogger(SomeFigurePattern.class);

    public static double getCx() {
        return cx;
    }

    public static double getCy() {
        return cy;
    }


    public SomeFigurePattern(double lineWidth, Color color, double x, double y) {
        super(lineWidth, color, x, y, FIGURE_TYPE_SOME_FIGURE);
    }

    static private double findMaxX()  {

        double maxX = 0;
            if (xArray == null || xArray.size() == 0) ;
            maxX = xArray.get(0);
            for (int i = 1; i < xArray.size(); i++) {
                if (maxX < xArray.get(i))
                    maxX = xArray.get(i);
            }
        return maxX;
    }

    static private double findMaxY()  {
        double maxY = yArray.get(0);
        for (int i = 1; i < yArray.size(); i++) {
            if (maxY < yArray.get(i))
                maxY = yArray.get(i);
        }
        return maxY;
    }


    static private double findMinX() throws IndexBoundException {
        double mixX = xArray.get(0);
        for (int i = 1; i < xArray.size(); i++) {
            if (mixX > xArray.get(i))
                mixX = xArray.get(i);
        }
        return mixX;
    }


    static private double findMinY() throws IndexBoundException {
        double minY = yArray.get(0);
        for (int i = 1; i < yArray.size(); i++) {
            if (minY > yArray.get(i))
                minY = yArray.get(i);
        }
        return minY;
    }

    public static void setFigure(ArrayList<Double> xx, ArrayList<Double> yy) {
        yArray = yy;
        xArray = xx;
        cx = (findMaxX() - findMinX()) / 2;
        cy = (findMaxY() - findMinY()) / 2;
    }

}
